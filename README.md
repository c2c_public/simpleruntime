El uso es:

# No se deben ejecutar con el usuario root

#Instalar Docker https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es
(Salir y volver a entrar)

# Para que pueda iniciar elasticseach
echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

docker login git.gob.cl:4567/simple/simple -u mparedes -p gfJ4wXhCA8xQexhvxHv7

# Clonamos los repositorios

git clone git@git.gob.cl:simple/simple.git
git clone git@gitlab.com:c2c_public/simpleruntime.git


# Node JS 10
sudo apt install build-essential apt-transport-https lsb-release ca-certificates curl
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install -y nodejs
node -v
npm -v

# Compilando los sitios 
sudo apt-get -y install libpng16-16 libpng-tools libpng-dev

git clone git@gitlab.com:c2c_public/simpleruntime.git
git clone git@git.gob.cl:simple/simple.git
cd simple

npm install
npm run prod

# Configurando Simple

cd ../simpleruntime
docker-compose up
docker-compose exec simple composer install
docker-compose exec simple php artisan migrate --seed
docker-compose exec simple vendor/bin/phpunit

docker-compose exec simple php artisan elasticsearch:admin create
docker-compose exec simple php artisan elasticsearch:admin index

docker-compose exec simple chown -R www-data:www-data /var/www/simple/storage
docker-compose exec simple chown -R www-data:www-data /var/www/simple/bootstrap

docker-compose exec simple php artisan simple:frontend frontend@c2c.cl 123456 1
docker-compose exec simple php artisan simple:backend backend@c2c.cl 123456
docker-compose exec simple php artisan simple:manager manager 123456

